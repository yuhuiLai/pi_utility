#include <unistd.h>
#include <cstdlib>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>

namespace
{
        const char *const pass = "passphrase_passwd=afj;#$ER"; // root密碼
}


static void make_pass()
{
        int fd = open("/tmp/passwd_file.txt", O_WRONLY | O_CREAT, 0600);
        write( fd, pass, strlen(pass));
        close(fd);
}

static void release_pass()
{
        unlink("/tmp/passwd_file.txt");
}

int main()
{
        make_pass();
        system("mount -t ecryptfs /root/.ftp /icerapi/ > /dev/null 2>&1");
        release_pass();
        return 0;
}

