import os

tty = "ttyS2"

class Resource:
    def __enter__(self):
        print( "open {0}".format(tty) )
        self.fd = open( "/dev/{0}".format(tty), 'rb')
        return self.fd

    def __exit__(self, exc_type, exc_val, exc_tb):
        print( "close {0}".format(tty) )
        self.fd.close()

def stty():
    command = "stty -F /dev/{0} 19200 cs8 -cstopb -parenb -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke"
    os.system( command.format(tty) )

def read_16(fd):
    return [ hex(byte) for byte in fd.read(16) ]

def one_task():
    counter = 0
    with Resource() as fd:
        while True:
            counter += 1
            print( counter )
            print( read_16(fd) )

def read_forever():
    while True:
        one_task()


if __name__ == '__main__':
    stty()
    read_forever()



