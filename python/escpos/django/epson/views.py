# coding=utf-8
import json
from django.shortcuts import render
from django.http import HttpResponse
from escpos.printer import Usb

# Create your views here.
template = u"""
ＸＸ股份有限公司(特店名稱) 
悠遊卡交易證明(顧客聯)

門  市: 悠遊門市
收銀機號： {0}
收銀機交易序號： {1}
交易時間： {2}
收銀員： {3}
悠遊卡卡號： {4}
交易類別： {5}

悠遊卡設備編號： {6}
批次號碼： {7}
RRN： {8}

交易前餘額： {9}
自動加值： {10}
交易金額： {11}
交易後餘額： {12}

娃娃機：{13}
備註：{14}
"""

def epson_format(recv):
    return template.format( 
    recv.get("cashier_machine_id",""), 
    recv.get("trans_id",""),
    recv.get("date_time",""),
    recv.get("cashier",""),
    recv.get("card_id",""), 
    recv.get("trans_type",""),
    recv.get("uu_machine_id",""),
    recv.get("batch_id",""),
    recv.get("RRN",""),
    recv.get("pre_trans_balance",""),
    recv.get("auto_bonus",""),
    recv.get("trans_amount",""),
    recv.get("after_trans_balance",""),
    recv.get("wawa_machine_id",""),
    recv.get("remarks",""))


def send_to_printer(data):
    #print data
    p = Usb( 0x04b8, 0x0e1f, 0)
    p._raw( data.encode("big5") )
    p.cut()

def epson(request):
    recv = json.loads( request.body.decode("utf-8") )
    send_to_printer( epson_format(recv) )
    return HttpResponse( json.dumps( recv ), content_type="application/json")


    