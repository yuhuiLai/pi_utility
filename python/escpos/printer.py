#!/usr/bin/env python
# coding=utf-8

from escpos.printer import Usb

template = u"""
ＸＸ股份有限公司(特店名稱) 
悠遊卡交易證明(顧客聯)

門  市: 悠遊門市
收銀機號： {0}
收銀機交易序號： {1}
交易時間： {2}
收銀員： {3}
悠遊卡卡號： {4}
交易類別： {5}

悠遊卡設備編號： {6}
批次號碼： {7}
RRN： {8}

交易前餘額： {9}
自動加值： {10}
交易金額： {11}
交易後餘額： {12}

備註：若有疑問請洽
悠遊卡公司專線：412-8880
(手機及金馬地區請加 02)
"""

def epson_format():
    return template.format( 
    u"99", 
    u"9999", 
    u"9999/99/99 99:99:99", 
    u"999", 
    u"(註 1)", 
    u"現金加值、扣款", 
    u"(註 2)", 
    u"(註 3)", 
    u"(註 4)", 
    u"$100", 
    u"$500 (註 5)", 
    u"$550", 
    u"$50").encode("big5")


def send_to_printer(data):
    print data
    p = Usb( 0x04b8, 0x0e1f, 0)
    p._raw( data )
    p.cut()
    p.close()


send_to_printer( epson_format() )





### example
#p.text("\n")
#p.image("./writer.gif")