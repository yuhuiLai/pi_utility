#!/usr/bin/env python3

import io
import sys
import traceback
import json
import os
import urllib.request
import re

from __future__ import print_function
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

def make_pair(line):
    tokens = line.split(" ")
    #print(tokens)
    if( len(tokens) >= 2 and tokens[1]=="*****"):
        return ( tokens[2], " ".join(tokens) )
    else:
        return ( None, None )

map = {}
netMap = {}
def map_push(line):
    key,value = make_pair(line)
    if( key and key not in map ):
        map[key] = []

    if(key and key not in netMap):
        netMap[key]=[]

    if( key ):
        map[key].append( value )
        netMap[key].append(value)

def map_len():
    result = 0
    for key in map:
        result = result + len( map[key] )
    return result

def map_write():
    for key in map:
        file_name = "./log/" + key
        with open( file_name, "a", encoding='UTF-8') as fd:
            fd.writelines( map[key] )
        

def map_comsumer():
    if( map_len() >= 100 ):
        map_write()
        map.clear()

def stdout(stream,line):
    stream.write(line)
    stream.flush()

def getCpuID():
    pattern=re.compile(r'[a-z|A-Z|0-9]{20}')
    result=os.popen("cat /proc/cpuinfo")
    lines=result.read()
    m=pattern.search(str(lines))
    cpuid=m.group().upper()
    return cpuid

#serverUrl="http://httpbin.org/post"
#serverUrl="http://report.signage-cloud.org:8700"
serverUrl="http://118.163.215.164:8700"
def postLog():
    # 資料json化
    logData = {getCpuID():netMap}
    jsonData = json.dumps(logData, ensure_ascii=False).encode('utf8')
    # 將資料加入 POST 
    req=urllib.request.Request(serverUrl)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    try:
        result=urllib.request.urlopen(req,jsonData,timeout=3)
        #print("###post return###"+result.read().decode('utf8'))
    except:
        print("can not post")
    netMap.clear()

def googleDrive():
    API_KEY = 'AIzaSyB_qEd4s3QZ-iUxtqrQK5hAnBCkzTNpL4Y'
    service = build('drive', 'v3', developerKey=API_KEY)

def diskout(line):
    map_push(line)
    map_comsumer()

def netout(line):
    postLog()

def main():
    input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
    output_stream = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    while True:
        line = input_stream.readline()
        stdout( output_stream, line)
        diskout(line)
        netout(line)

try:
    main()
except Exception as e:
    print( e.__doc__ )
    #print( e.message )
    print( traceback.format_exc() )
