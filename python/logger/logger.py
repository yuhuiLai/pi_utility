#!/usr/bin/env python3

import io
import sys
import traceback
import json
import os
import urllib.request
import re

class LogMapControl():

    locMap={}
    netMap={}

    def make_pair(self,line):
        tokens = line.split(" ")
        #print(tokens)
        if( len(tokens) >= 2 and tokens[1]=="*****"):
            return ( tokens[2], " ".join(tokens) )
        else:
            return ( None, None )


    def map_push(self,line):
        key,value = self.make_pair(line)
        if( key and key not in self.locMap ):
            self.locMap[key] = []
        if( key and key not in self.netMap ):
            self.netMap[key] = []
        if( key ):
            self.locMap[key].append( value )
            self.netMap[key].append( value )

    def map_len(self,map):
        result = 0
        for key in map:
            result = result + len( map[key] )
        return result

    def map_write(self):
        for key in self.locMap:
            file_name = "./log/" + key
            with open( file_name, "a", encoding='UTF-8') as fd:
                fd.writelines( self.locMap[key] )       

    def map_comsumer(self):
        if( self.map_len(self.locMap) >= 100 ):
            self.map_write()
            self.locMap.clear()

    def getNetMap(self):
        return self.netMap

    def netMapClear(self):
        self.netMap.clear()

class NetworkControl():

    serverUrl="http://httpbin.org/post"
    #serverUrl="http://118.163.215.164:8700"

    def postLog(self,map):
        # 資料json化
        logData = {getCpuID():map}
        jsonData = json.dumps(logData, ensure_ascii=False).encode('utf8')
        # 將資料加入 POST 
        req=urllib.request.Request(self.serverUrl)
        req.add_header('Content-Type', 'application/json; charset=utf-8')
        try:
            result=urllib.request.urlopen(req,jsonData,timeout=3)
            print("###post return###"+result.read().decode('utf8'))
        except:
            print("can not post")

def stdout(stream,line):
    stream.write(line)
    stream.flush()

def getCpuID():
    pattern=re.compile(r'[a-z|A-Z|0-9]{20}')
    result=os.popen("cat /proc/cpuinfo")
    lines=result.read()
    m=pattern.search(str(lines))
    cpuid=m.group().upper()
    return cpuid

def netOut(map):
    networkControl=NetworkControl()
    networkControl.postLog(map)

def main():
    input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
    output_stream = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

    logMapControl=LogMapControl()

    while True:
        line = input_stream.readline()
        stdout( output_stream, line)

        logMapControl.map_push(line)
        logMapControl.map_comsumer()

        if logMapControl.map_len(logMapControl.getNetMap())>=1:
            netOut(logMapControl.getNetMap())
            logMapControl.netMapClear()

try:
    main()
except Exception as e:
    print( e.__doc__ )
    #print( e.message )
    print( traceback.format_exc() )
