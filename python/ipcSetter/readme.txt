1. 執行此程式需要python函式庫:
	paramiko
	boto3
	botocore

2. 對config.py按右鍵編輯，說明以後補充

3. 推送json檔主程式為jsonSender.py，直接按兩下執行，執行前先把output資料夾
清空，執行完會在裡面生成資料夾以此形式命名"ip@cpuid"，若失敗會在資料夾名稱
中加上"error"

4. wifi設定主程式為wifiSetter