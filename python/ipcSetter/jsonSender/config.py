########### 你的電腦IP(執行這個程式的電腦)，你必須與IPC連同一個網路(相同區域網路)#########
def localIp():
    return '192.168.81.200'

########### 初始值:255，如果只想設定單一IPC，若他的IP是192.168.1.15，這裡填IP第4位，15 ###################################
def ipRangeMax():
    return 185

########### 初始值:-1，如果只想設定單一IPC，若他的IP是192.168.1.15，這裡填IP第4位-1，15-1=14 #############################
def ipRangeMin():
    return 184

########### 保姆機IP###############
def babysitterIp():
    return "192.168.81.100"

########### 程式執行時是否刪除資料，1=要刪除 0=不刪除 ##########
def deleteFlag():
    return 1

########### 程式執行時是否更新updater.jar與updater.properties，1=要 0=不要###########
def updateFlag():
    return 1

########### 程式執行時是否修改wifi，1=要 0=不要###########
def wifiFlag():
    return 1

########### 修改ipc wifi ssid #############
def wifiSsid():
    return "paypc"

########### 修改ipc wifi password #########
def wifiPassword():
    return "abcd1234"

########### 正式或測試環境，正式=0 測試=1 #########
def uatFlag():
    return 1






def localIpLast():
    return localIp().split('.',3)[3]

def localIpNoLast():
    return localIp().split('.',3)[0]+'.'+localIp().split('.',3)[1]+'.'+localIp().split('.',3)[2]+'.'