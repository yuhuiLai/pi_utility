import paramiko
import config
import socket
import connectAzS3
import time
import os
import checkFile
import fileWrite
import shutil
import reboot

errorList=[]
cpuid=""
errorCount=0
lastError=""

def pingTest(ip):
    #ping test
    #socket初始化
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #2 Second Timeout
    sock.settimeout(2)
    response = sock.connect_ex((ip,22))  
    return response

def setLoop():
    ipLast=config.ipRangeMin()
    localLast=config.localIpLast()
    localNoLast=config.localIpNoLast()

    while ipLast<config.ipRangeMax():
        ipLast+=1
        ip=localNoLast+str(ipLast)
        #ping test 成功
        if pingTest(ip) == 0:
            print(ip+"ping success\n")

            sshConnect(ip)
        else:
            print(ip+"ping error\n")


def sshConnect(ip):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    try:
        #ssh連線
        ssh.connect(ip,22,username='root',password='afj;#$ER',timeout=4)
        sftp = ssh.open_sftp()
        print("ssh start\n")

        #清除舊資料
        if config.deleteFlag()==1:
            stdin, stdout, stderr=ssh.exec_command('find /icerapi/ -type f ! -name "updater.jar" ! -name "updater.properties" ! -name "a.out" ! -name "paypayrest" ! -name "start.sh" -exec rm -rf {} \;')
            time.sleep(1)
            stdin, stdout, stderr=ssh.exec_command('sync')
            print(ip+"delete end\n")
                
        #取得cpuid
        cpuid=""
        stdin, stdout, stderr=ssh.exec_command('curl http://127.0.0.1:9527/?API=CPUID')
        stdout.channel.recv_exit_status()
        lines = stdout.readlines()
        for line in lines:
            if len(line)>20:
                cpuid=line[10:30]
                print(cpuid)
                #下載json
                connectAzS3.downloadJson(cpuid)  
        if cpuid=="":
            print("這台抓不到cpuid\n")

        #寫入保姆機IP
        fileWrite.switchBabysitterIP()       
                
        #傳送檔案
        print(ip+"send start\n")
        sftp.put('json\\'+cpuid+'\\app.security.setting.json', '/icerapi/app.security.setting.json')
        sftp.put('json\\'+cpuid+'\\updater.security.setting.json', '/icerapi/updater.security.setting.json')
        if config.updateFlag()==1:
            sftp.put('updater\\updater.jar','/icerapi/updater.jar')
            sftp.put('updater\\updater.properties','/icerapi/updater.properties')
        #time.sleep(2)
        stdin, stdout, stderr=ssh.exec_command('sync')
        stdin, stdout, stderr=ssh.exec_command('sync')        
        print(ip+"put success\n") 

        #檢查檔案是否成功傳送
        if os.path.exists('output\\'+ip+'@'+cpuid) == False:
            os.makedirs('output\\'+ip+'@'+cpuid)
        sftp.get('/icerapi/updater.jar','output\\'+ip+'@'+cpuid+'\\updater.jar')
        sftp.get('/icerapi/updater.properties','output\\'+ip+'@'+cpuid+'\\updater.properties',)
        sftp.get('/icerapi/a.out','output\\'+ip+'@'+cpuid+'\\a.out')
        sftp.get('/icerapi/start.sh','output\\'+ip+'@'+cpuid+'\\start.sh',)
        sftp.get('/icerapi/paypayrest','output\\'+ip+'@'+cpuid+'\\paypayrest')
        sftp.get('/icerapi/app.security.setting.json','output\\'+ip+'@'+cpuid+'\\app.security.setting.json',)
        sftp.get('/icerapi/updater.security.setting.json','output\\'+ip+'@'+cpuid+'\\updater.security.setting.json',)
        print(ip+"check success\n")   

        #修改wifi
        if config.wifiFlag()==1:
            print(ip+"wifi set start")
            try:
                fileWrite.switchWifi()
                sftp.put('wifi.sh', '/home/mx1600/test.sh') 
                time.sleep(1)
                stdin, stdout, stderr=ssh.exec_command('nohup sh /home/mx1600/test.sh')
                #time.sleep(3)
                print(ip+"wifi set\n")
            except:
                print(ip+"wifi set\n")

        ssh.close()

    except :
        #錯誤IPC識別，錯誤3次內重試
        print("錯誤次數:"+str(errorCount)+"\n")
        if errorCount==0:
            lastError=ip
        elif errorCount==3:
            checkFile.errorDef(ip,cpuid)
            errorList.append(cpuid)
            errorCount=0
        if lastError==ip:
            errorCount+=1
            ipLast-=1 

        print(ip+"ssh error\n")     
        ssh.close()



######  main

try:
    shutil.rmtree("output")
    #shutil.rmtree("json")
except:
    print("")
 
setLoop()
print("錯誤列表:")
print(errorList)

print("已完成")
input()



'''
#4線程IP指派
ipLast=config.ipRangeMin()-3
#4線程
sshThread1 = threading.Thread(target=sshJsonSender,args=(ipLast,))
sshThread2 = threading.Thread(target=sshJsonSender,args=(ipLast+1,))
sshThread3 = threading.Thread(target=sshJsonSender,args=(ipLast+2,))
sshThread4 = threading.Thread(target=sshJsonSender,args=(ipLast+3,))

sshThread1.start()
sshThread2.start()
sshThread3.start()
sshThread4.start()
'''


