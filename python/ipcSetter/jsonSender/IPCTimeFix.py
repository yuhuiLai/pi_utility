import paramiko
import config
import socket
import time

errorList=[]

def pingTest(ip):
    #ping test
    #socket初始化
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #2 Second Timeout
    sock.settimeout(2)
    response = sock.connect_ex((ip,22))  
    return response

def sshJsonSender(ipLast):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    localLast=config.localIpLast()
    localNoLast=config.localIpNoLast()

    cpuid=""
    errorCount=0
    lastError=0
    
    while ipLast < config.ipRangeMax(): 
        ipLast+=1
        ip=localNoLast+str(ipLast)

        #ping test 成功
        if pingTest(ip) == 0:
            print(ip+"ping success\n")
            
            try:
                #ssh連線
                ssh.connect(ip,22,username='root',password='afj;#$ER',timeout=4)
                sftp = ssh.open_sftp()
                print("ssh start\n")

                nowDate = time.strftime("%m/%d/%Y",time.localtime())
                print("nowDate:"+nowDate)
                nowTime = time.strftime("%H:%M:%S",time.localtime())
                print("nowTime:"+nowTime)
                stdin, stdout, stderr=ssh.exec_command('sudo date -s '+nowDate)
                stdin, stdout, stderr=ssh.exec_command('sudo date -s '+nowTime)
                stdin, stdout, stderr=ssh.exec_command('sync')
                print("time已完成")
            except :
                ssh.close()                
        else:
            print(ip+"ping error\n")

######  main

try:
    print("")
except:
    print("")

ipLast=config.ipRangeMin()
while True:
    sshJsonSender(ipLast)
    print("錯誤列表:")
    print(errorList)

    print("已完成")

input()

