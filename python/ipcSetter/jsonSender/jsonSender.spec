# -*- mode: python -*-

block_cipher = None


a = Analysis(['D:\\UserData\\Documents\\GitRepo\\pi_utility\\python\\ipcSetter\\jsonSender\\jsonSender.py'],
             pathex=['C:\\Program Files (x86)\\Microsoft Visual Studio\\Shared\\Python36_64\\Scripts'],
             binaries=[],
             datas=[],
             hiddenimports=['configparser'],
             hookspath=[],
             runtime_hooks=[],
             excludes=['config'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='jsonSender',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='jsonSender')
