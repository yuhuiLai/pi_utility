import config
import checkFile

def switchBabysitterIP():
    uatFlag="false"
    if config.uatFlag()==1:
        uatFlag="true"
    # 開啟檔案
    fp = open("updater/updater.properties", "w") 
    # 寫入檔案
    fp.write("uat_enabled="+uatFlag+"\nbabysitter_address="+config.babysitterIp()+"\nbabysitter_port=16000\nshow_log_on_console=false")
    # 關閉檔案
    fp.close()

def switchWifi():
    # 開啟檔案
    fp = open("wifi.sh", "wb") 
    # 寫入檔案
    fp.write(("nmcli c delete test\nnmcli c delete paypc\nnmcli c add type wifi con-name test ifname wlan0 ssid '"+config.wifiSsid()+"'\nnmcli con modify test wifi-sec.key-mgmt wpa-psk\nnmcli con modify test wifi-sec.psk '"+config.wifiPassword()+"'\nnmcli con up test\nsync\n"+checkFile.ipcLedBlueOff()+"\n"+checkFile.ipcLedWhiteOff()+"\n").encode())
    # 關閉檔案
    fp.close()