import paramiko
import time
import os

def errorDef(ip,cpuid):
    if os.path.exists('output\\'+ip+'error@'+cpuid) == False:
        os.makedirs('output\\'+ip+'error@'+cpuid)

def ipcLedBlueOn():
    return "curl \'http://127.0.0.1:9527/?API=GPIODN&PIN=1\'"

def ipcLedBlueOff():
    return "curl \'http://127.0.0.1:9527/?API=GPIOUP&PIN=1\'"

def ipcLedGreenOn():
    return "curl \'http://127.0.0.1:9527/?API=GPIODN&PIN=16\'"

def ipcLedGreenOff():
    return "curl \'http://127.0.0.1:9527/?API=GPIOUP&PIN=16\'"

def ipcLedOrangeOn():
    return "curl \'http://127.0.0.1:9527/?API=GPIODN&PIN=18\'"

def ipcLedOrangeOff():
    return "curl \'http://127.0.0.1:9527/?API=GPIOUP&PIN=18\'"

def ipcLedWhiteOn():
    return "curl \'http://127.0.0.1:9527/?API=GPIODN&PIN=22\'"

def ipcLedWhiteOff():
    return "curl \'http://127.0.0.1:9527/?API=GPIOUP&PIN=22\'"

#print(ipcLedWhiteOff())